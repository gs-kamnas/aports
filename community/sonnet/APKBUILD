# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=sonnet
pkgver=5.106.0
pkgrel=0
pkgdesc="Spelling framework for Qt5"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends="hunspell"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	hunspell-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/sonnet-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# sonnet-test_autodetect fails to detect a speller backend
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "sonnet-test_autodetect"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dbcb20e6dbb8d7de2690734bbf931d94db0454ab11f5607ac2ed8acfd89edde8adaf86e5966cb30a7c1789d750f28d8fd9d03a141f8a7fca2a3d061e8adfc454  sonnet-5.106.0.tar.xz
"
