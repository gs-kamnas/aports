# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=py3-cattrs
pkgver=23.1.1
pkgrel=0
pkgdesc="Complex custom class converters for attrs"
url="https://github.com/python-attrs/cattrs"
arch="noarch"
license="MIT"
depends="python3 py3-attrs"
makedepends="
	py3-gpep517
	py3-poetry-core
	"
options="!check" # loads of missing deps
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/c/cattrs/cattrs-$pkgver.tar.gz"
builddir="$srcdir/cattrs-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 setup.py test
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
2df582d9ce8fdb0a70591cf391e78b7f670fc5f2810417cb3fb19695afaafc3baadf59200d400a429f69fdc1c7fd76fe3c723f4b367889354ab5bb81a7439747  cattrs-23.1.1.tar.gz
"
