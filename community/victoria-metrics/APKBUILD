# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>
pkgname=victoria-metrics
pkgver=1.91.0
pkgrel=0
provides="victoriametrics=$pkgver-r$pkgrel"
pkgdesc="Fast, cost-effective and scalable time series database"
url="https://github.com/VictoriaMetrics/VictoriaMetrics"
# Test failures on other archs, FP precision mostly.
arch="x86_64 aarch64"
license="Apache-2.0"
makedepends="go"
subpackages="$pkgname-openrc $pkgname-tools:_tools $pkgname-backup-tools:_backup_tools"
install="$pkgname.pre-install"
source="$pkgname-$pkgver.tar.gz::https://github.com/VictoriaMetrics/VictoriaMetrics/archive/v$pkgver.tar.gz
	$pkgname.initd
	$pkgname.confd
	"
builddir="$srcdir/VictoriaMetrics-$pkgver"

# Depends on cgo zstd package that ships prebuilt object files built against glibc.
# It is possible to rebuild it against musl, but
# pure go zstd lib works well enough and is not written in C.
export CGO_ENABLED=0

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local ldflags="-X github.com/VictoriaMetrics/VictoriaMetrics/lib/buildinfo.Version=$pkgver"
	local bin
	for bin in victoria-metrics vmagent vmalert vmauth vmctl vmrestore vmbackup; do
		go build -ldflags="$ldflags" -o bin/$bin ./app/$bin
	done
}

check() {
	# Rounding issues in sinh() and TestExecExprSuccess.
	# Other 2 tests are flaky.
	go test -short -skip '^TestExecExprSuccess$|^TestExecSuccess$/^sinh|^Test_vmNativeProcessor_run$|^TestTableCreateSnapshotAt$' ./...
}

package() {
	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname

	cd bin
	install -Dm755 -t "$pkgdir"/usr/bin \
		victoria-metrics \
		vmagent \
		vmalert \
		vmauth \
		vmbackup \
		vmctl \
		vmrestore
}

_tools() {
	pkgdesc="$pkgdesc (vmagent, vmalert, vmauth, vmctl)"
	amove \
		usr/bin/vmagent \
		usr/bin/vmalert \
		usr/bin/vmauth \
		usr/bin/vmctl
}

_backup_tools() {
	pkgdesc="$pkgdesc (vmbackup, vmrestore)"
	amove \
		usr/bin/vmbackup \
		usr/bin/vmrestore
}

sha512sums="
e87055b5445442c70674bcd35572de64d67bb68e4ecf436b6ab954abeb1837ff6b9f7528e0e1298ca26604ed46035a7dc2c0b326ae97261e35959037b74fbe94  victoria-metrics-1.91.0.tar.gz
d727de5653e0ed9c7d7448dce6ab3766683d14e8d946935929691709c8b077572d5eb73c26749593cd7995820d370d46851be487fc03b663e495129c6a9e0244  victoria-metrics.initd
19a4d53d60459e9a0fd3f57e5a16f0425ad634cfa494f6f676f561793db879627dfcead1cbe1ee3be8843fdda7454e4ae0bbd49a1e8f041079efa639d416eee3  victoria-metrics.confd
"
