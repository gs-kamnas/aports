# Maintainer: Bart Ribbers <bribbers@disroot.org>
# Contributor: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivities-stats
pkgver=5.106.0
pkgrel=0
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
pkgdesc="A library for accessing the usage data collected by the activities system"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="boost-dev kconfig-dev kactivities-dev graphviz-dev qt5-qttools-dev qt5-qtdeclarative-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qtbase-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-stats-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
e35a36ad9b4237427e897ff1bfdd3ceb91037ac19eb8a6e2d9327e8c8868ddac81dade4196161ac116d9bf07178191221ce9075c7f9da3d351ed5d5c7764cff4  kactivities-stats-5.106.0.tar.xz
"
