# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=incidenceeditor
pkgver=23.04.1
pkgrel=0
pkgdesc="KDE PIM incidence editor"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	calendarsupport-dev
	eventviews-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kdiagram-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kldap-dev
	kmailtransport-dev
	kmime-dev
	libkdepim-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/incidenceeditor-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# akonadi-sqlite-incidencedatetimetest and akonadi-mysql-incidencedatetimetest require running DBus
	# ktimezonecomboboxtest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(akonadi-(sqlite|mysql)-incidencedatetime|ktimezonecombobox)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
9cf209288239d455736fc2edb90987e95a8441c628010731546f84c19cb85a38b8f206df83e849a0678c58571dc877fa90682898287d6552fcd90a20f55b482f  incidenceeditor-23.04.1.tar.xz
"
