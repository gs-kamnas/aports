# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesdk-kio
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development"
pkgdesc="KIO-Slaves"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kio-dev
	perl-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdesdk-kio-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
47bbf272079a6219ff3c4a2cacbf7e979c7e5b7d2fd533b25502a17d124cd65b2dfc34cbed1b504f092371d9c5d3bf8c2e89b0c32c19b0542e50855d75e62d30  kdesdk-kio-23.04.1.tar.xz
"
